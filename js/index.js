var arrNumbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
var arrLowerCase = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
var arrUpperCase = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
var arrSpecialSymbol = ['!', '&', '$', '@', '?', '*', '#', '%', '/'];

document.getElementById('length-password').oninput = function (){
    // ползунок - длина массива
    document.getElementById('password-length').innerHTML = this.value;
 }

// generatePass(); // запуск при старте
document.getElementById('generator').onclick = generatePass;

function generatePass() {
    var result = [];
    if (document.getElementById('numbers').checked) {
        // включены ли цифры
        result = result.concat(arrNumbers);
    }
    if (document.getElementById('lower-case').checked) {
        // включены ли строчные буквы
        result = result.concat(arrLowerCase);
    }
    if (document.getElementById('upper-case').checked) {
        // включены ли прописные буквы

        result = result.concat(arrUpperCase);
    }
    if (document.getElementById('special-symbol').checked) {
        // включены ли спецсимволы

        result = result.concat(arrSpecialSymbol);
    }
    result.sort(compareRandom); // перемешиваю результ массив
    // console.log(result);
    document.getElementById('out').innerHTML = '';
    for (var k = 0; k < 6; k++) {

        var pass = ''; // будущий пароль
        var passLen = parseInt(document.getElementById('length-password').value); // длина пароля
        for (var i = 0; i < passLen; i++) {
            // цикл по дляне пароля
            // выбираю случайное значение из массива result
            pass += result[randomInteger(0, result.length - 1)];
        }
        // console.log(pass);
        document.getElementById('out').innerHTML += '<p>' + pass + '</p>';
    }

}

function compareRandom(a, b) {
    return Math.random() - 0.5;
}

function randomInteger (min, max) {
    var rand = min - 0.5 + Math.random() * (max - min +1);
    rand = Math.round(rand);
    return rand;
}